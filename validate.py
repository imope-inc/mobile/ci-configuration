#!/usr/bin/env python3
#
# script to validate .gitlab-ci.yml
#
import json
import sys

import requests


def main():
  with open(sys.argv[1]) as f:
    r = requests.post("https://gitlab.com/api/v4/ci/lint",
          json={ 'content': f.read() }, verify=True)

  if r.status_code != requests.codes['OK']:
    sys.exit(3)

  data = r.json()
  if data['status'] != 'valid':
    for e in data['errors']:
      print(e, file=sys.stderr)
    sys.exit(1)

  sys.exit(0) # valid

if __name__ == '__main__':
  main()
